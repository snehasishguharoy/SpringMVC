/**
 * 
 */
package com.spring.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author sony
 *
 */
@Controller
public class HelloController {

	@RequestMapping("/welcome")
	public ModelAndView hello() {

		String message = "Welcome to the Kolkata";
		return new ModelAndView("hello", "message", message.toUpperCase());
	}

	@RequestMapping("/index")
	public ModelAndView loadHomePage() {
		return new ModelAndView("abc");
	}

}
